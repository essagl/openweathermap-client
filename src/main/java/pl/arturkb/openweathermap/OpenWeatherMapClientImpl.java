package pl.arturkb.openweathermap;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class OpenWeatherMapClientImpl implements OpenWeatherMapClient {

    private static final String PROTOCOL = "http://";
    @SuppressWarnings("SpellCheckingInspection")
    private static final String API_KEY_PREFIX = "&APPID=";

    private final HttpClient httpClient;

    /**
     * API key from the service.
     */
    private final String apiKey;

    /**
     * Base url;
     */
    private final String baseUrl;

    private OpenWeatherMapClientImpl(Builder builder) {
        this.baseUrl = builder.getBaseUrl();
        this.apiKey = builder.getApiKey();
        this.httpClient = HttpClient.newHttpClient();
    }

    public HttpResponse<String> get(String uri) {
        HttpRequest request = HttpRequest.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .GET()
                .uri(URI.create(retrieveFullUri(uri)))
                .build();

        try {
            return httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            throw new WeatherClientRequestException(e);
        }
    }

    private String retrieveFullUri(String uri) {
        return PROTOCOL + baseUrl + uri + API_KEY_PREFIX + apiKey;
    }

    public static class Builder {
        private String apiKey;
        private String baseUrl;

        public Builder withApiKey(String apiKey) {
            this.apiKey = apiKey;
            return this;
        }

        public Builder withBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
            return this;
        }

        String getApiKey() {
            return apiKey;
        }

        String getBaseUrl() {
            return baseUrl;
        }

        public OpenWeatherMapClientImpl build() {
            return new OpenWeatherMapClientImpl(this);
        }
    }

}

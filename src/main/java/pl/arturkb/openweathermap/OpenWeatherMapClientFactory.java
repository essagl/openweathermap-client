package pl.arturkb.openweathermap;

public class OpenWeatherMapClientFactory {

    private static OpenWeatherMapClient instance;

    public static OpenWeatherMapClient getInstance() {
        if (instance == null) {
            instance = new OpenWeatherMapClientImpl.Builder()
                    .withApiKey(ApiEnum.API_KEY.getValue())
                    .withBaseUrl(ApiEnum.BASE_URI.getValue())
                    .build();
        }
        return instance;
    }

    public static OpenWeatherMapClient getInstance(String baseUri, String apiKey) {
        if (instance == null) {
            instance = new OpenWeatherMapClientImpl.Builder()
                    .withApiKey(apiKey)
                    .withBaseUrl(baseUri)
                    .build();
        }
        return instance;
    }

}

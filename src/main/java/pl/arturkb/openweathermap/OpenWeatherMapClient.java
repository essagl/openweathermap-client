package pl.arturkb.openweathermap;

import java.net.http.HttpResponse;

public interface OpenWeatherMapClient {

    HttpResponse<String> get(String uri);

}

package pl.arturkb.openweathermap.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Objects;

@JsonDeserialize(builder = System.Builder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class System {

    /**
     * Internal parameter
     */
    private final int type;

    /**
     * Internal parameter
     */
    private final int id;

    /**
     * Internal parameter
     */
    private final float message;

    /**
     * Country code (GB, JP etc.)
     */
    private final String country;

    /**
     * Sunrise time, unix, UTC
     */
    private final long sunrise;

    /**
     * Sunset time, unix, UTC
     */
    private final long sunset;

    private System(Builder builder) {
        this.type = builder.type;
        this.id = builder.id;
        this.message = builder.message;
        this.country = builder.country;
        this.sunrise = builder.sunrise;
        this.sunset = builder.sunset;
    }

    public static Builder make() {
        return new Builder();
    }

    public static Builder copy(System other) {
        return new Builder(other);
    }

    @SuppressWarnings("WeakerAccess")
    public int getType() {
        return type;
    }

    @SuppressWarnings("WeakerAccess")
    public int getId() {
        return id;
    }

    @SuppressWarnings("WeakerAccess")
    public float getMessage() {
        return message;
    }

    @SuppressWarnings("WeakerAccess")
    public String getCountry() {
        return country;
    }

    @SuppressWarnings("WeakerAccess")
    public long getSunrise() {
        return sunrise;
    }

    @SuppressWarnings("WeakerAccess")
    public long getSunset() {
        return sunset;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof System)) return false;
        System system = (System) o;
        return type == system.type &&
                id == system.id &&
                sunrise == system.sunrise &&
                sunset == system.sunset &&
                message == system.message &&
                Objects.equals(country, system.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, id, message, country, sunrise, sunset);
    }

    public static class Builder {
        private int type;
        private int id;
        private float message;
        private String country;
        private long sunrise;
        private long sunset;

        Builder() {
        }

        Builder(System other) {
            if (other == null) {
                throw new NullPointerException("Can't copy from null: System.Builder");
            } else {
                this.type = other.getType();
                this.id = other.getId();
                this.message = other.getMessage();
                this.country = other.getCountry();
                this.sunrise = other.getSunrise();
                this.sunset = other.getSunset();
            }
        }

        public System build() {
            return new System(this);
        }

        public Builder withType(int type) {
            this.type = type;
            return this;
        }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withMessage(float message) {
            this.message = message;
            return this;
        }

        public Builder withCountry(String country) {
            this.country = country;
            return this;
        }

        public Builder withSunrise(long sunrise) {
            this.sunrise = sunrise;
            return this;
        }

        public Builder withSunset(long sunset) {
            this.sunset = sunset;
            return this;
        }
    }

}

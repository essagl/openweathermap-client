package pl.arturkb.openweathermap.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Objects;

@JsonDeserialize(builder = Wind.Builder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Wind {

    /**
     * Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
     */
    private final float speed;

    /**
     * Wind direction, degrees (meteorological)
     */
    private final float degrees;

    private Wind(Builder builder) {
        this.speed = builder.speed;
        this.degrees = builder.degrees;
    }

    public static Builder make() {
        return new Builder();
    }

    public static Builder copy(Wind other) {
        return new Builder(other);
    }

    @SuppressWarnings("WeakerAccess")
    public float getSpeed() {
        return speed;
    }

    @SuppressWarnings("WeakerAccess")
    @JsonProperty("deg")
    public float getDegrees() {
        return degrees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Wind)) return false;
        Wind wind = (Wind) o;
        return Float.compare(wind.speed, speed) == 0 &&
                Float.compare(wind.degrees, degrees) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(speed, degrees);
    }

    public static class Builder {
        private float speed;
        private float degrees;

        Builder() {
        }

        Builder(Wind other) {
            if (other == null) {
                throw new NullPointerException("Can't copy from null: Wind.Builder");
            } else {
                this.speed = other.getSpeed();
                this.degrees = other.getDegrees();
            }
        }

        public Builder withSpeed(float speed) {
            this.speed = speed;
            return this;
        }

        @JsonProperty("deg")
        public Builder withDegrees(float degrees) {
            this.degrees = degrees;
            return this;
        }

        public Wind build() {
            return new Wind(this);
        }

    }
}

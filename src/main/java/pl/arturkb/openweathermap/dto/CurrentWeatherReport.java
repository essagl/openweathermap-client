package pl.arturkb.openweathermap.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Current weather report
 * <p>
 * Class that represents json response mapping from call to current weather data api.
 * <p>
 * Access current weather data for any location including over 200,000 cities
 * Current weather is frequently updated based on global models and data from more than 40,000 weather stations
 * Data is available in JSON, XML, or HTML format
 * Available for Free and all other paid accounts
 *
 * @see <a href="https://openweathermap.org/current">Current weather data api.</a>
 */
@JsonDeserialize(builder = CurrentWeatherReport.Builder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CurrentWeatherReport {

    /**
     * Geographic coordinates
     */
    private final Coordinates coordinates;

    /**
     * Weather-condition codes
     */
    private final Set<WeatherConditionCode> weatherConditionCodes;

    /**
     * Internal parameter
     */
    private final String base;

    private final MainWeather mainWeather;

    /**
     * Visibility, meter
     */
    private final float visibility;

    private final Wind wind;

    private final Clouds clouds;

    /**
     * Time of data calculation, unix, UTC
     */
    private final long timeOfDataCalculation;

    private final System system;
    private final long timezone;
    private final Rain rain;
    private final Snow snow;

    /**
     * The name of the city
     */
    private final String cityName;

    /**
     * Id of the city
     */
    private final long cityId;

    /**
     * Internal parameter.
     */
    private final long cod;
    private final String message;

    private CurrentWeatherReport(Builder builder) {
        this.coordinates = builder.coordinates;
        this.weatherConditionCodes = builder.weatherConditionCodes;
        this.base = builder.base;
        this.mainWeather = builder.mainWeather;
        this.visibility = builder.visibility;
        this.wind = builder.wind;
        this.clouds = builder.clouds;
        this.timeOfDataCalculation = builder.timeOfDataCalculation;
        this.system = builder.system;
        this.timezone = builder.timezone;
        this.cityName = builder.cityName;
        this.cityId = builder.cityId;
        this.cod = builder.cod;
        this.message = builder.message;
        this.rain = builder.rain;
        this.snow = builder.snow;
    }

    public static Builder make() {
        return new Builder();
    }

    public static Builder copy(CurrentWeatherReport other) {
        return new Builder(other);
    }

    /**
     * Getter for geographic coordinate
     *
     * @return geographic coordinate
     */
    @SuppressWarnings({"unused", "SpellCheckingInspection"})
    @JsonProperty("coord")
    public Coordinates getCoordinates() {
        return coordinates;
    }

    @SuppressWarnings("unused")
    @JsonProperty("weather")
    public Set<WeatherConditionCode> getWeatherConditionCode() {
        return weatherConditionCodes;
    }

    /**
     * Getter for internal parameter
     *
     * @return internal parameter
     */
    @SuppressWarnings("unused")
    public String getBase() {
        return base;
    }

    @SuppressWarnings("unused")
    @JsonProperty("main")
    public MainWeather getMainWeather() {
        return mainWeather;
    }

    @SuppressWarnings("unused")
    public float getVisibility() {
        return visibility;
    }

    @SuppressWarnings("unused")
    public Wind getWind() {
        return wind;
    }

    @SuppressWarnings("unused")
    public Clouds getClouds() {
        return clouds;
    }

    @SuppressWarnings("unused")
    @JsonProperty("dt")
    public long getTimeOfDataCalculation() {
        return timeOfDataCalculation;
    }

    @SuppressWarnings("unused")
    @JsonProperty("sys")
    public System getSystem() {
        return system;
    }

    @SuppressWarnings("unused")
    public long getTimezone() {
        return timezone;
    }

    @SuppressWarnings("unused")
    @JsonProperty("name")
    public String getCityName() {
        return cityName;
    }

    /**
     * Getter for internal parameter cod
     *
     * @return the internal parameter cod
     */
    @SuppressWarnings("unused")
    public long getCod() {
        return cod;
    }

    @SuppressWarnings("unused")
    public String getMessage() {
        return message;
    }

    /**
     * Getter for city Id. City id is internal id of service.
     *
     * @return the city id
     */
    @SuppressWarnings("unused")
    @JsonProperty("id")
    public long getCityId() {
        return cityId;
    }

    @SuppressWarnings("unused")
    public Rain getRain() {
        return rain;
    }

    @SuppressWarnings("unused")
    public Snow getSnow() {
        return snow;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CurrentWeatherReport)) return false;
        CurrentWeatherReport that = (CurrentWeatherReport) o;
        return Float.compare(that.visibility, visibility) == 0 &&
                timeOfDataCalculation == that.timeOfDataCalculation &&
                timezone == that.timezone &&
                cityId == that.cityId &&
                cod == that.cod &&
                Objects.equals(coordinates, that.coordinates) &&
                Objects.equals(weatherConditionCodes, that.weatherConditionCodes) &&
                Objects.equals(base, that.base) &&
                Objects.equals(mainWeather, that.mainWeather) &&
                Objects.equals(wind, that.wind) &&
                Objects.equals(clouds, that.clouds) &&
                Objects.equals(system, that.system) &&
                Objects.equals(cityName, that.cityName) &&
                Objects.equals(rain, that.rain) &&
                Objects.equals(snow, that.snow) &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinates, weatherConditionCodes, base, mainWeather, visibility, wind, clouds,
                timeOfDataCalculation, system, timezone, cityName, cityId, cod, rain, snow, message);
    }

    public static class Builder {
        private Coordinates coordinates;
        private Set<WeatherConditionCode> weatherConditionCodes;
        private String base;
        private MainWeather mainWeather;
        private float visibility;
        private Wind wind;
        private Clouds clouds;
        private long timeOfDataCalculation;
        private System system;
        private long timezone;
        private String cityName;
        private long cityId;
        private long cod;
        private String message;
        private Rain rain;
        private Snow snow;

        Builder() {
            weatherConditionCodes = new HashSet<>();
        }

        Builder(CurrentWeatherReport other) {
            if (other == null) {
                throw new NullPointerException("Can't copy from null");
            } else {
                this.coordinates = other.coordinates == null ? null : Coordinates.copy(other.coordinates).build();
                this.weatherConditionCodes = getWeatherConditionCodesCopy(other.weatherConditionCodes);
                this.base = other.getBase();
                this.mainWeather = other.mainWeather == null ? null : MainWeather.copy(other.mainWeather).build();
                this.visibility = other.visibility;
                this.wind = other.wind == null ? null : Wind.copy(other.wind).build();
                this.clouds = other.clouds == null ? null : Clouds.copy(other.clouds).build();
                this.timeOfDataCalculation = other.timeOfDataCalculation;
                this.system = other.system == null ? null : System.copy(other.system).build();
                this.timezone = other.timezone;
                this.cityName = other.getCityName();
                this.cityId = other.getCityId();
                this.cod = other.getCod();
                this.message = other.getMessage();
                this.rain = other.rain == null ? null : Rain.copy(other.rain).build();
                this.snow = other.snow == null ? null : Snow.copy(other.snow).build();
            }
        }

        private Set<WeatherConditionCode> getWeatherConditionCodesCopy(Set<WeatherConditionCode> others) {
            if (others == null) {
                return null;
            } else if (others.isEmpty()) {
                return new HashSet<>();
            } else {
                Set<WeatherConditionCode> weatherConditionCodesCopy = new HashSet<>();
                others.forEach(original -> weatherConditionCodesCopy.add(WeatherConditionCode.copy(original).build()));
                return weatherConditionCodesCopy;
            }
        }

        @SuppressWarnings("SpellCheckingInspection")
        @JsonProperty("coord")
        public Builder withCoordinates(Coordinates coordinates) {
            this.coordinates = coordinates;
            return this;
        }

        @SuppressWarnings("unused")
        @JsonProperty("weather")
        public Builder withWeatherConditionCode(Set<WeatherConditionCode> weatherConditionCodes) {
            this.weatherConditionCodes = weatherConditionCodes;
            return this;
        }

        public Builder addWeatherConditionCode(WeatherConditionCode weatherConditionCode) {
            weatherConditionCodes.add(weatherConditionCode);
            return this;
        }

        public Builder withBase(String base) {
            this.base = base;
            return this;
        }

        @JsonProperty("main")
        public Builder withMainWeather(MainWeather mainWeather) {
            this.mainWeather = mainWeather;
            return this;
        }

        public Builder withVisibility(float visibility) {
            this.visibility = visibility;
            return this;
        }

        public Builder withWind(Wind wind) {
            this.wind = wind;
            return this;
        }

        public Builder withClouds(Clouds clouds) {
            this.clouds = clouds;
            return this;
        }

        @JsonProperty("dt")
        public Builder withTimeOfDataCalculation(long timeOfDataCalculation) {
            this.timeOfDataCalculation = timeOfDataCalculation;
            return this;
        }

        @JsonProperty("sys")
        public Builder withSystem(System system) {
            this.system = system;
            return this;
        }

        public Builder withTimezone(long timezone) {
            this.timezone = timezone;
            return this;
        }

        @JsonProperty("name")
        public Builder withCityName(String cityName) {
            this.cityName = cityName;
            return this;
        }

        @JsonProperty("id")
        public Builder withCityId(long cityId) {
            this.cityId = cityId;
            return this;
        }

        public Builder withRain(Rain rain) {
            this.rain = rain;
            return this;
        }

        public Builder withSnow(Snow snow) {
            this.snow = snow;
            return this;
        }

        public Builder withCod(long cod) {
            this.cod = cod;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public CurrentWeatherReport build() {
            return new CurrentWeatherReport(this);
        }

    }
}

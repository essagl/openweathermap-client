package pl.arturkb.openweathermap.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Objects;

@JsonDeserialize(builder = Clouds.Builder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Clouds {

    private final int cloudiness;

    private Clouds(Builder builder) {
        this.cloudiness = builder.cloudiness;
    }

    public static Builder make() {
        return new Builder();
    }

    public static Builder copy(Clouds other) {
        return new Builder(other);
    }

    @SuppressWarnings("unused")
    @JsonProperty("all")
    public int getCloudiness() {
        return cloudiness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Clouds)) return false;
        Clouds clouds = (Clouds) o;
        return cloudiness == clouds.cloudiness;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cloudiness);
    }

    public static class Builder {
        private int cloudiness;

        @JsonProperty("all")
        public Builder withCloudiness(int cloudiness) {
            this.cloudiness = cloudiness;
            return this;
        }

        Builder() {
        }

        Builder(Clouds other) {
            if (other == null) {
                throw new NullPointerException("Can't copy from null: Clouds.Builder");
            } else {
                this.cloudiness = other.cloudiness;
            }
        }

        public Clouds build() {
            return new Clouds(this);
        }

    }

}

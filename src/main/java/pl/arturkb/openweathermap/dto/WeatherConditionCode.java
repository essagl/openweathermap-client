package pl.arturkb.openweathermap.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Objects;

/**
 * @see <a href="https://openweathermap.org/weather-conditions">Weather-conditions.</a>
 */
@JsonDeserialize(builder = WeatherConditionCode.Builder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WeatherConditionCode {

    private final int id;
    private final String main;
    private final String description;
    private final String icon;

    private WeatherConditionCode(Builder builder) {
        this.id = builder.id;
        this.main = builder.main;
        this.description = builder.description;
        this.icon = builder.icon;
    }

    public static Builder make() {
        return new Builder();
    }

    public static Builder copy(WeatherConditionCode other) {
        return new Builder(other);
    }

    @SuppressWarnings("WeakerAccess")
    public int getId() {
        return id;
    }

    @SuppressWarnings("WeakerAccess")
    public String getMain() {
        return main;
    }

    @SuppressWarnings("WeakerAccess")
    public String getDescription() {
        return description;
    }

    @SuppressWarnings("WeakerAccess")
    public String getIcon() {
        return icon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WeatherConditionCode)) return false;
        WeatherConditionCode that = (WeatherConditionCode) o;
        return id == that.id &&
                Objects.equals(main, that.main) &&
                Objects.equals(description, that.description) &&
                Objects.equals(icon, that.icon);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, main, description, icon);
    }

    public static class Builder {
        private int id;
        private String main;
        private String description;
        private String icon;

        Builder() {
        }

        Builder(WeatherConditionCode other) {
            if (other == null) {
                throw new NullPointerException("Can't create copy of null: WeatherConditionCode.Builder");
            } else {
                this.id = other.getId();
                this.main = other.getMain();
                this.description = other.getDescription();
                this.icon = other.getIcon();
            }
        }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withMain(String main) {
            this.main = main;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withIcon(String icon) {
            this.icon = icon;
            return this;
        }

        public WeatherConditionCode build() {
            return new WeatherConditionCode(this);
        }

    }

}

package pl.arturkb.openweathermap.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Objects;

/**
 * Class that represents geographic coordinates
 */
@JsonDeserialize(builder = Coordinates.Builder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Coordinates {

    private final float latitude;
    private final float longitude;

    private Coordinates(Builder builder) {
        this.latitude = builder.latitude;
        this.longitude = builder.longitude;
    }

    public static Builder make() {
        return new Builder();
    }

    public static Builder copy(Coordinates other) {
        return new Builder(other);
    }

    @SuppressWarnings("WeakerAccess")
    @JsonProperty("lat")
    public float getLatitude() {
        return latitude;
    }

    @SuppressWarnings("WeakerAccess")
    @JsonProperty("lon")
    public float getLongitude() {
        return longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coordinates)) return false;
        Coordinates that = (Coordinates) o;
        return Float.compare(that.latitude, latitude) == 0 &&
                Float.compare(that.longitude, longitude) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }

    public static class Builder {
        private float latitude;
        private float longitude;

        Builder() {
        }

        Builder(Coordinates other) {
            if (other == null) {
                throw new NullPointerException("Can't create copy from null: Coordinates.Builder");
            } else {
                this.latitude = other.getLatitude();
                this.longitude = other.getLongitude();
            }
        }

        @JsonProperty("lat")
        public Builder withLatitude(float latitude) {
            this.latitude = latitude;
            return this;
        }

        @JsonProperty("lon")
        public Builder withLongitude(float longitude) {
            this.longitude = longitude;
            return this;
        }

        public Coordinates build() {
            return new Coordinates(this);
        }

    }
}

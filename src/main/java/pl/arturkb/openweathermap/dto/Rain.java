package pl.arturkb.openweathermap.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Objects;

@JsonDeserialize(builder = Rain.Builder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Rain {

    /**
     * Rain volume for the last 1 hour, mm
     */
    private final int rainIn1h;


    /**
     * Rain volume for the last 3 hours, mm
     */
    private final int rainIn3h;

    private Rain(Builder builder) {
        this.rainIn1h = builder.rainIn1h;
        this.rainIn3h = builder.rainIn3h;
    }

    public static Builder make() {
        return new Builder();
    }

    public static Builder copy(Rain other) {
        return new Builder(other);
    }

    @SuppressWarnings("WeakerAccess")
    @JsonProperty("rain.1h")
    public int getRainIn1h() {
        return rainIn1h;
    }

    @SuppressWarnings("WeakerAccess")
    @JsonProperty("rain.3h")
    public int getRainIn3h() {
        return rainIn3h;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rain)) return false;
        Rain rain = (Rain) o;
        return rainIn1h == rain.rainIn1h &&
                rainIn3h == rain.rainIn3h;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rainIn1h, rainIn3h);
    }

    public static class Builder {
        private int rainIn1h;
        private int rainIn3h;

        Builder() {

        }

        Builder(Rain other) {
            if (other == null) {
                throw new NullPointerException("Can't copy from null: Rain.Builder");
            } else {
                this.rainIn1h = other.getRainIn1h();
                this.rainIn3h = other.getRainIn3h();
            }
        }

        @JsonProperty("rain.1h")
        public Builder withRainIn1h(int rainIn1h) {
            this.rainIn1h = rainIn1h;
            return this;
        }

        @JsonProperty("rain.3h")
        public Builder withRainIn3h(int rainIn3h) {
            this.rainIn3h = rainIn3h;
            return this;
        }

        public Rain build() {
            return new Rain(this);
        }

    }

}

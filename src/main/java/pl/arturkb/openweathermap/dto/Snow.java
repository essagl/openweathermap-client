package pl.arturkb.openweathermap.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Objects;

@JsonDeserialize(builder = Snow.Builder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Snow {

    /**
     * Snow volume for the last 1 hour, mm
     */
    private final int snowIn1h;


    /**
     * Snow volume for the last 3 hours, mm
     */
    private final int snowIn3h;

    private Snow(Builder builder) {
        this.snowIn1h = builder.snowIn1h;
        this.snowIn3h = builder.snowIn3h;
    }

    public static Builder make() {
        return new Builder();
    }

    public static Builder copy(Snow other) {
        return new Builder(other);
    }

    @SuppressWarnings("WeakerAccess")
    @JsonProperty("snow.1h")
    public int getSnowIn1h() {
        return snowIn1h;
    }

    @SuppressWarnings("WeakerAccess")
    @JsonProperty("snow.3h")
    public int getSnowIn3h() {
        return snowIn3h;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Snow)) return false;
        Snow snow = (Snow) o;
        return snowIn1h == snow.snowIn1h &&
                snowIn3h == snow.snowIn3h;
    }

    @Override
    public int hashCode() {
        return Objects.hash(snowIn1h, snowIn3h);
    }

    public static class Builder {
        private int snowIn1h;
        private int snowIn3h;

        Builder() {

        }

        Builder(Snow other) {
            if (other == null) {
                throw new NullPointerException("Can't copy from null: Snow.Builder");
            } else {
                this.snowIn1h = other.getSnowIn1h();
                this.snowIn3h = other.getSnowIn3h();
            }
        }

        @JsonProperty("snow.1h")
        public Builder withSnowIn1h(int snowIn1h) {
            this.snowIn1h = snowIn1h;
            return this;
        }

        @JsonProperty("snow.3h")
        public Builder withSnowIn3h(int snowIn3h) {
            this.snowIn3h = snowIn3h;
            return this;
        }

        public Snow build() {
            return new Snow(this);
        }

    }

}

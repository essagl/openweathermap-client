package pl.arturkb.openweathermap.api;

import org.apache.commons.lang3.StringUtils;
import pl.arturkb.openweathermap.OpenWeatherMapClient;
import pl.arturkb.openweathermap.dto.CurrentWeatherReport;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.Optional;

import static pl.arturkb.openweathermap.utils.MappingUtils.getObject;

public class CurrentWeatherDataSync implements CurrentWeatherData {

    private final OpenWeatherMapClient openWeatherMapClient;
    private final boolean useMetric;

    public CurrentWeatherDataSync(OpenWeatherMapClient openWeatherMapClient, boolean useMetric) {
        this.openWeatherMapClient = openWeatherMapClient;
        this.useMetric = useMetric;
    }

    @Override
    public Optional<CurrentWeatherReport> byCityName(String cityName, String countryCode) throws IOException {
        if (StringUtils.isBlank(cityName)) {
            return Optional.empty();
        }

        StringBuilder uriBuilder = new StringBuilder()
                .append("/data/2.5/weather")
                .append("?q=")
                .append(cityName);

        if (StringUtils.isNoneBlank(countryCode)) {
            uriBuilder.append(",").append(countryCode);
        }

        if (useMetric) {
            uriBuilder.append("&units=metric");
        }

        final HttpResponse<String> stringHttpResponse = openWeatherMapClient.get(uriBuilder.toString());
        if (stringHttpResponse.statusCode() == 200) {
            return Optional.of(getObject(stringHttpResponse.body(), CurrentWeatherReport.class));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<CurrentWeatherReport> byGeographicCoordinates(float latitude, float longitude) throws IOException {

        StringBuilder uriBuilder = new StringBuilder()
                .append("/data/2.5/weather")
                .append("?lat=")
                .append(latitude)
                .append("&lon=")
                .append(longitude);
        if (useMetric) {
            uriBuilder.append("&units=metric");
        }

        final HttpResponse<String> stringHttpResponse = openWeatherMapClient.get(uriBuilder.toString());
        if (stringHttpResponse.statusCode() == 200) {
            return Optional.of(getObject(stringHttpResponse.body(), CurrentWeatherReport.class));
        } else {
            return Optional.empty();
        }

    }
}

package pl.arturkb.openweathermap;

public enum ApiEnum {

    BASE_URI("openweathermap.org"),
    API_KEY("9f1f736a16e4068a68021f77e47bbdf6");

    private final String value;

    ApiEnum(String value) {
        this.value = value;
    }

    String getValue() {
        return value;
    }

}
